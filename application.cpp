#include <stdint.h>
#include <string.h>

#include <Wire.h>

#include "raat.hpp"
#include "raat-buffer.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

#include "http-get-server.hpp"

#include "application.hpp"
#include "standard_erm.hpp"

static HTTPGetServer s_server(NULL);
static const raat_devices_struct * s_pDevices = NULL;

static bool s_pressed_flag = false;

void ethernet_packet_handler(char * req)
{
    bool handled = false;

    if (!handled)
    {
    	handled = intro_button_handle_req(s_server, req);
    }

    if (!handled)
    {
    	handled = standard_erm_handle_req(s_server, req);
    }
}

char * ethernet_response_provider()
{
    return s_server.get_response();
}

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices; (void)params;

    standard_erm_setup(devices, params);
    
    raat_logln_P(LOG_APP, PSTR("Carfax Button/Relays: ready"));
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)params;
    intro_button_loop(devices, params);
    maglock_button_loop(devices, params);
}
