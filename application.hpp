#ifndef _APPLICATION_H_
#define _APPLICATION_H_

void intro_button_loop(const raat_devices_struct& devices, const raat_params_struct& params);
bool intro_button_handle_req(HTTPGetServer& server, char * req);

void maglock_button_loop(const raat_devices_struct& devices, const raat_params_struct& params);

#endif
