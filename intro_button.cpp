/* C/C++ Includes */

#include <stdint.h>
#include <string.h>

/* RAAT Includes */

#include "raat.hpp"
#include "raat-buffer.hpp"

/* Library Includes */

#include "http-get-server.hpp"

/* Application Includes */

#include "application.hpp"
#include "standard_erm.hpp"

/* Private Variables */

static bool s_pressed_flag = false;

static const char BUTTON_STATUS_URL[] PROGMEM = "/button/status";

/* HTTP Request Handling */

static void switch_state_req_handler(HTTPGetServer& server, char const * const url)
{
    (void)url;
    Serial.println(F("Handling /button/status"));
    standard_erm_send_std_response(server);
    server.add_body_P(check_and_clear(s_pressed_flag) ? PSTR("PRESSED") : PSTR("NOT PRESSED"));
}

static http_get_handler s_handlers[] = 
{
    {BUTTON_STATUS_URL, switch_state_req_handler},
    {"", NULL}
};

/* Public Functions */

bool intro_button_handle_req(HTTPGetServer& server, char * req)
{
    return server.handle_req(s_handlers, req);
}

void intro_button_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)params;
    s_pressed_flag = !devices.pInputs[0]->state();
}
