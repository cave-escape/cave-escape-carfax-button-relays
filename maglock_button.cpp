/* C/C++ Includes */

#include <stdint.h>
#include <string.h>

/* RAAT Includes */

#include "raat.hpp"
#include "raat-buffer.hpp"

/* Library Includes */

#include "http-get-server.hpp"
#include "standard_erm.hpp"

/* Application Includes */

#include "application.hpp"

/* Public Functions */

void maglock_button_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)params;
    if (devices.pInputs[1]->check_low_and_clear())
    {
    	raat_logln(LOG_APP, "Switch pressed, turning off maglocks");
        devices.pOutputs[2]->set(false);
        devices.pOutputs[3]->set(false);
    }
}
