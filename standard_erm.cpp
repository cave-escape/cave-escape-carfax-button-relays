/* C/C++ Includes */

#include <stdint.h>
#include <string.h>

/* RAAT Includes */

#include "raat.hpp"
#include "raat-buffer.hpp"

/* Library Includes */

#include "http-get-server.hpp"
#include "standard_erm.hpp"

static raat_devices_struct * s_pDevices = NULL;

static uint8_t s_min_input_pin = UINT8_MAX;
static uint8_t s_max_input_pin = 0;
static uint8_t s_min_output_pin = UINT8_MAX;
static uint8_t s_max_output_pin = 0;

typedef void (*timed_output_setter_fn)(int32_t, int32_t);
typedef void (*nontimed_output_setter_fn)(int32_t);

static void do_set(int32_t output_pin)
{
    raat_logln_P(LOG_APP, PSTR("Setting output %" PRIi32), output_pin);
    uint8_t output_pin_idx = output_pin - s_min_output_pin;
    s_pDevices->pOutputs[output_pin_idx]->set(true); 
}

static void do_toggle(int32_t output_pin)
{
    raat_logln_P(LOG_APP, PSTR("Toggling output %" PRIi32), output_pin);
    uint8_t output_pin_idx = output_pin - s_min_output_pin;
    s_pDevices->pOutputs[output_pin_idx]->toggle(); 
}

static void do_clear(int32_t output_pin)
{
    raat_logln_P(LOG_APP, PSTR("Clearing output %" PRIi32), output_pin);
    uint8_t output_pin_idx = output_pin - s_min_output_pin;
    s_pDevices->pOutputs[output_pin_idx]->set(false);
}

static void do_timed_set(int32_t output_pin, int32_t timeout)
{
    raat_logln_P(LOG_APP, PSTR("Setting output %" PRIi32), output_pin);
    uint8_t output_pin_idx = output_pin - s_min_output_pin;
    s_pDevices->pOutputs[output_pin_idx]->set(true, timeout);
}

static void do_timed_toggle(int32_t output_pin, int32_t timeout)
{
    raat_logln_P(LOG_APP, PSTR("Toggling output %" PRIi32), output_pin);
    uint8_t output_pin_idx = output_pin - s_min_output_pin;
    s_pDevices->pOutputs[output_pin_idx]->toggle(timeout); 
}

static void do_timed_clear(int32_t output_pin, int32_t timeout)
{
    raat_logln_P(LOG_APP, PSTR("Clearing output %" PRIi32), output_pin);
    uint8_t output_pin_idx = output_pin - s_min_output_pin;
    s_pDevices->pOutputs[output_pin_idx]->set(false, timeout);
}

static void get_input(HTTPGetServer& server, char const * const url, char const * const end)
{
    int32_t input_pin;
    char const * const pInputPin = end + 1;

    bool success = false;

    standard_erm_send_std_response(server);

    if ((success = raat_parse_single_numeric(pInputPin, input_pin, NULL)))
    {
        success = inrange((uint8_t)input_pin, s_min_input_pin, s_max_input_pin);

        if (success)
        {
            uint8_t input_pin_idx = input_pin - s_min_input_pin;
            if (s_pDevices->pInputs[input_pin_idx]->state())
            {
                server.add_body_P(PSTR("1\r\n\r\n"));
            }
            else
            {
                server.add_body_P(PSTR("0\r\n\r\n"));   
            }
        }
        else
        {
            raat_logln_P(LOG_APP, PSTR("Input number %d out of range %d-%d"), (uint8_t)input_pin, s_min_input_pin, s_max_input_pin);
        }
    }
    else
    {
        raat_logln_P(LOG_APP, PSTR("Could not parse input number from %s"), end);
    }

    if (!success)
    {
        server.add_body_P(PSTR("?\r\n\r\n"));
    }
}

static void set_bool_response(HTTPGetServer& server, bool success)
{
    if (success)
    {
        server.add_body_P(PSTR("OK\r\n\r\n"));
    }
    else
    {
        server.add_body_P(PSTR("?\r\n\r\n"));
    }
}

static void nontimed_output_handler(HTTPGetServer& server, char const * const pOutputUrl, nontimed_output_setter_fn pOutputSetterFn)
{
    int32_t output_pin;

    bool success = false;

    standard_erm_send_std_response(server);

    if ((success = raat_parse_single_numeric(pOutputUrl, output_pin, NULL)))
    {
        if (success = inrange((uint8_t)output_pin, s_min_output_pin, s_max_output_pin))
        {
            pOutputSetterFn(output_pin);
        }
    }
    set_bool_response(server, success);
}

static void timed_output_handler(HTTPGetServer& server, char const * const pOutputUrl, timed_output_setter_fn pOutputSetterFn)
{
    int32_t output_pin;
    int32_t timeout;

    char * pTime = NULL;

    bool success = false;

    standard_erm_send_std_response(server);

    if ((success = raat_parse_single_numeric(pOutputUrl, output_pin, &pTime)))
    {
        if ((success = raat_parse_single_numeric(pTime+1, timeout, NULL)))
        {
            success = inrange((uint8_t)output_pin, s_min_output_pin, s_max_output_pin);
            success &= timeout > 100;

            if (success)
            {
                pOutputSetterFn(output_pin, timeout);
            }
        }
    }
    set_bool_response(server, success);
}

static void set_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)url;
    nontimed_output_handler(server, end+1, do_set);
}

static void toggle_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)url;
    nontimed_output_handler(server, end+1, do_toggle);
}

static void clear_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)url;
    nontimed_output_handler(server, end+1, do_clear);
}

static void timed_set_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)url;
    timed_output_handler(server, end+1, do_timed_set);
}


static void timed_toggle_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)url;
    timed_output_handler(server, end+1, do_timed_toggle);
}

static void timed_clear_output(HTTPGetServer& server, char const * const url, char const * const end)
{
    (void)url;
    timed_output_handler(server, end+1, do_timed_clear);
}

static const char GET_INPUT_URL[] PROGMEM = "/input/get";
static const char SET_OUTPUT_URL[] PROGMEM = "/output/set";
static const char TOGGLE_OUTPUT_URL[] PROGMEM = "/output/toggle";
static const char CLEAR_OUTPUT_URL[] PROGMEM = "/output/clear";
static const char TIMED_SET_OUTPUT_URL[] PROGMEM = "/output/timedset";
static const char TIMED_TOGGLE_OUTPUT_URL[] PROGMEM = "/output/timedtoggle";
static const char TIMED_CLEAR_OUTPUT_URL[] PROGMEM = "/output/timedclear";

static http_get_handler s_handlers[] = 
{
    {GET_INPUT_URL, get_input},
    {SET_OUTPUT_URL, set_output},
    {TOGGLE_OUTPUT_URL, toggle_output},
    {CLEAR_OUTPUT_URL, clear_output},
    {TIMED_SET_OUTPUT_URL, timed_set_output},
    {TIMED_TOGGLE_OUTPUT_URL, timed_toggle_output},
    {TIMED_CLEAR_OUTPUT_URL, timed_clear_output},
    {"", NULL}
};

/* Public Functions */

bool standard_erm_handle_req(HTTPGetServer& server, char * req)
{
    return server.handle_req(s_handlers, req);
}

void standard_erm_send_std_response(HTTPGetServer& server)
{
    server.set_response_code_P(PSTR("200 OK"));
    server.set_header_P(PSTR("Access-Control-Allow-Origin"), PSTR("*"));
    server.finish_headers();
}

void standard_erm_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    s_pDevices = &devices;

    s_min_input_pin = 0;
    s_max_input_pin = devices.InputsCount-1;
    s_min_output_pin = 3;
    s_max_output_pin = 3 + devices.OutputsCount - 1;
}
