#ifndef _STANDARD_ERM_H_
#define _STANDARD_ERM_H_

bool standard_erm_handle_req(HTTPGetServer& server, char * req);
void standard_erm_send_std_response(HTTPGetServer& server);
void standard_erm_setup(const raat_devices_struct& devices, const raat_params_struct& params);

#endif
